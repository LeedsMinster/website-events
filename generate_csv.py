#!/usr/bin/env python3

"""Generate CSV files of events for uploading to WordPress"""

# Imports
import argparse
import datetime
from pathlib import Path

ODIR = Path.home() / 'leeds_minster_events'
EVENTS_PER_FILE = 5
HEADERS = ['Event Name', 'Event Start Date', 'Event Start Time',
           'Event End Date', 'Event End Time', 'Event Category']
DATE_FORMAT = '%Y-%m-%d'
DAY = datetime.timedelta(days=1)

class Event:

    def __init__(self, title, weekday, start_hour, start_minute, end_hour,
                 end_minute, categories=None):
        self.title = title
        self.weekday = weekday
        self.start_time = f'{start_hour:02d}:{start_minute:02d}:00'
        self.end_time = f'{end_hour:02d}:{end_minute:02d}:00'
        if categories is None:
            self.categories = ''
        else:
            self.categories = '"' + ','.join(categories) + '"'

    def date_is_event(self, date):
        """Determine whether this event happens on the given date"""
        return date.isoweekday() == self.weekday

    def write(self, fout, date):
        """Write event to *fout* and return True; else, return False"""
        date_str = date.strftime(DATE_FORMAT)
        info = [self.title, date_str, self.start_time, date_str, self.end_time,
                self.categories]
        fout.write(','.join(info) + '\n')


def friday_organ_recital():
    html = """<!-- ()

-->Free admission with retiring collection
"""
    return Event('Organ Recital', 5, 13, 0, 13, 45, ['Music']), html

def sunday_organ_recital():
    html = """<!-- ()

-->Tickets £5 on the door
"""
    return Event('Organ Recital', 7, 17, 30, 18, 30, ['Music']), html

def sunday_said_eucharist():
    html = """<em>Book of Common Prayer</em><!--
<em></em>-->
"""
    return Event('Said Holy Communion', 7, 9, 0, 9, 45, ['Services']), html

def sunday_choral_eucharist():
    html = """<em>Common Worship</em> 
<!--Preacher: <em></em>

Setting: <em></em> | 
Psalm: 
Anthem: <em></em> | 
Voluntary: <em></em> | -->
"""
    return Event('Choral Eucharist', 7, 10, 30, 11, 45,
                 ['Services', 'Choral', 'Music']), html

def sunday_choral_evensong():
    html = """<em>Book of Common Prayer</em>
<!--Preacher: <em></em>

Responses: 
Psalm: 
Canticles: <em></em> | 
Anthem: <em></em> | 
Voluntary: <em></em> | -->
"""
    return Event('Choral Evensong', 7, 17, 30, 18, 30,
                 ['Services', 'Choral', 'Music']), html

def thursday_said_eucharist():
    html = """<em>Common Worship</em>
"""
    return Event('Said Holy Communion', 4, 12, 0, 12, 45, ['Services']), html

def thursday_choral_evensong():
    html = """<em>Book of Common Prayer</em>
<!--
Responses: 
Psalm: 
Canticles: <em></em> | 
Anthem: <em></em> | 
Voluntary: <em></em> | -->
"""
    return Event('Choral Evensong', 4, 19, 0, 19, 45,
                 ['Services', 'Choral', 'Music']), html

def create_csvs(opath, event, curr_date, end_date):
    """Create a CSV file for the given events"""

    # Iterate for each day
    opath = str(opath)
    while curr_date <= end_date:
    
        # Make list of next dates
        dates = []
        while len(dates) < EVENTS_PER_FILE:
            if event.date_is_event(curr_date):
                dates.append(curr_date)
            curr_date += DAY
            if curr_date > end_date:
                break

        # Create CSV file and write to it
        if dates:
            with open(opath.format(t1=dates[0], t2=dates[-1]), 'w') as fout:
                fout.write(','.join(HEADERS) + '\n')
                for date in dates:
                    event.write(fout, date)
            print(fout.name)

    
if __name__ == '__main__':

    func_dict = {
        'fri_organ': friday_organ_recital,
        'sun_organ': sunday_organ_recital,
        'sun_900': sunday_said_eucharist,
        'sun_1030': sunday_choral_eucharist,
        'sun_530': sunday_choral_evensong,
        'thu_1200': thursday_said_eucharist,
        'thu_700': thursday_choral_evensong,
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('event', choices=func_dict)
    str2yyyymmdd = lambda s: datetime.datetime.strptime(s, '%Y%m%d')
    parser.add_argument('start_date', type=str2yyyymmdd, help='YYYYMMDD')
    parser.add_argument('end_date', type=str2yyyymmdd, help='YYYYMMDD')
    args = parser.parse_args()

    event, html = func_dict[args.event]()

    ODIR.mkdir(exist_ok=True)
    opath = ODIR / f'{args.event}_{{t1:%Y%m%d}}_{{t2:%Y%m%d}}.csv'
    print()
    create_csvs(opath, event, args.start_date, args.end_date)
    print()
    print('HTML for Event thumbnail intro:')
    print()
    print(html)
